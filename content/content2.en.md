---
title: 'Status Wetter'
weight: 2
date: 2018-12-06T09:29:16+10:00
align: left
---

Wenn das Anmeldedatum fix ist, kann es passieren das die Aktion trotzdem wegen schlechtem
 Wetter (Schnee, Hagel, Regen, Sturm) verschoben wird. Dann wird das hier angezeigt am Tag X. 
