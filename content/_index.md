---
title: 'Home'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'Die Toten der Festung Europa '
heroSubHeading: 'Die Kundgebung "Ein Licht, ein Leben", fand erstmals Anfang 2021 statt. Es sollten die gestorbenen Geflüchteten in Europa und auf dem Weg dahin sichtbar gemacht werden. Dazu sollte für jeden Toten ein Teelicht angezündet werden. In dem Buch "Todesursache Flucht", sind 35.500 Namen aufgelistet mit der Todesursache und der Quelle.'
heroBackground: 'kerzen/kerzen.jpg'
---
