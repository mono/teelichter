---
title: 'Kundgebung Jahr 2021'
weight: 1
#background: 'kerzen/flag.png'
button: 'Dokumentation'
buttonLink: '/dokumentation/2021'
---

Am 23. Januar 2021 fand die stationäre Kundgebung zum Gedenken an verstorbene Geflüchtete in Bremen auf der Bürgerweide statt. Ungefähr 100 selbstorganisierte Aktivisten haben dort ca. 10.000 Teelichter angezündet, die mit Brottüten vorm Wind geschützt wurden. Dann gab es noch einen kurzen Redebeitrag, indem auf die Verantwortung der Politik und ihr absichtliches Versagen an den Grenzen mit der Konsequenz von bisher über 40.000 gestorbenen Geflüchteten in und um Europa eingegangen wurde. Es ist geplant dieses Gedenken zu wiederholen, aber noch macht uns das Ordnungsamt Probleme.
