---
title: 'Our Event'
weight: 2
background: 'kerzen/flag.png'
button: 'Contact us'
buttonLink: 'about'
---
We invite you all to participate in the commemoration of the people who died on the run. Please bring candles, lighters and glasses and help to build and light the sea of lights.
Especially glasses for the tea lights are needed. Collection points have been set up for this purpose
