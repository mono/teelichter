---
title: 'Unsere Veranstalltung'
weight: 2
background: 'kerzen/flag.png'
button: 'Schreibe uns'
buttonLink: 'about'
---

Wir laden euch alle ein, an der Gedenkveranstaltung für die auf der Flucht gestorbenen Menschen, teilzunehmen. Bringt gerne Kerzen, Feuerzeuge und Gläser mit und helft dabei das Lichtermeer aufzubauen und zu entzünden.
Gebraucht werden vor allem Gläser für die Teelichter. Dazu wurden Sammelstellen eingerichtet
