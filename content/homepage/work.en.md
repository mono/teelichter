---
title: 'Rally Year 2021'
weight: 1
#background: 'kerzen/flag.png'
button: 'Documentation'
buttonLink: 'work/2021'
---

On January 23, 2021, the stationary rally in memory of deceased refugees in Bremen took place on the Bürgerweide. About 100 self-organized activists lit about 10,000 tea lights, which were protected from the wind with bread bags. Then there was a short speech, in which the responsibility of politics and their deliberate failure at the borders with the consequence of so far over 40,000 dead refugees in and around Europe was addressed. It is planned to repeat this commemoration, but we still have problems with the public order office.

