---
title: 'Fotos Januar 2021'
date: 2018-11-18T12:33:46+10:00
draft: false
weight: 1
heroHeading: 'Fotos Januar 2021'
heroSubHeading: 'von Anonym'
heroBackground: ''
thumbnail: 'work/schlachthof.jpg'
---

Angeregt durch eine Buch über gestorbene Geflüchtete organisierte ein Bremer Student eine Mahnwache mit 35 000 Teelichtern, um deutlich zu machen, dass hinter jeder Zahl, hinter jedem Namen ein Mensch steht, der nicht zuletzt durch unsere Gleichgültigkeit sein Leben verlor.
{{< youtube id="qNAhB_2dNDU" autoplay="true" >}}

Hier können die Fotos betrachtet werden [was ist das?](#vexant-achivi) ist das nicht schön?

{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview="true" loadJQuery=True >}}



Urheberrechte verbleiben bei den Fotografen.

