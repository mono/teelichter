---
title: 'January 2021' 
date: 2018-11-18T12:33:46+10:00
draft: false
weight: 1
heroHeading: 'Fotos Januar 2021'
heroSubHeading: 'Anonymous'
heroBackground: ''
thumbnail: 'work/schlachthof.jpg'
---
Inspired by a book about refugees who died, a student from Bremen organized a vigil with 35,000 tea lights to make clear that behind every number, behind every name, there is a human being who lost his life not least because of our indifference.

{{< youtube id="qNAhB_2dNDU" autoplay="true" >}}

Here you can view the photos:

{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview="true" loadJQuery=True >}}

Copyrights remain with the photographers.

