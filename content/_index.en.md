---
title: 'Home'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'The dead of fortress Europe'
heroSubHeading: 'The rally "One Light, One Life", first took place in early 2021. It was to make visible the refugees who died in Europe and on the way there. For this purpose, a tea light was to be lit for each dead person. In the book "Cause of Death Flight", 35,500 names are listed with the cause of death and the source.'
heroBackground: 'kerzen/kerzen.jpg'
---
