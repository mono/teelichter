---
title: 'Status of the rally'
date: 2018-12-06T09:29:16+10:00
weight: 1
align: right
---

The office of order makes stress. Actually, the action should take place between Christmas
and New Year's Eve take place. But the Bürgerweide is not free there according to the Ordnungsamt.
In addition, tea lights would be too dangerous and therefore only grave lights allowed.
But our action goes only with tea lights and we put these apart
and have fire extinguishers.
