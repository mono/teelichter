---
title: 'Status Anmeldung'
date: 2018-12-06T09:29:16+10:00
weight: 1
align: right
---

Das Ordungsamt macht streß. Eigentlich sollte die Aktion zwischen Weihnachten
und Sylvester stattfinden. Aber die Bürgerweide ist dort nicht frei laut Ordnungsamt.
Außerdem wären Teelichter zu gefährlich und deshalb nur Grablichter erlaubt.
Aber unsere Aktion geht nur mit Teelichtern und wir stellen diese Auseinander
und haben Feuerlöscher.
