---
title: 'Hintergründe'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-6.png'
draft: false
featured: true
weight: 2
heroHeading: 'first Flyer '
heroSubHeading: 'from Jan 2021'
heroBackground: 'work/flyer1.jpg'
---
In dem Buch: „Todesursache Flucht – eine unvollständige Liste“ sind 35.500 tote Geflüchtete aufgelistet - eine unvorstellbar große Zahl. Die Liste umfasst den Zeitraum von 1993 - 22.06.2018. Mittlerweile ist die Zahl auf über 40.000 angewachsen. Die Dunkelziffer liegt weitaus höher. Es handelt sich hier jedoch nicht um eine bloße Zahl, sondern um Menschen, deren Tod von der EU als Friedensnobelpreisträgerin billigend in Kauf genommen wurde. 
Am 23.01.21 von 17-20 Uhr möchte eine Gruppe ziviler Einzelpersonen den Toten auf der Bremer Bürgerweide gedenken. Dazu werden wir 35.500 Teelichter auf der Bürgerweide aufstellen und entzünden.
Wir laden euch alle ein, an der Gedenkveranstaltung für die auf der Flucht gestorbenen Menschen, teilzunehmen. Bringt gerne Kerzen, Feuerzeuge und Gläser mit und helft dabei das Lichtermeer aufzubauen und zu entzünden.
Gebraucht werden vor allem Gläser für die Teelichter. Dazu wurden Sammelstellen eingerichtet. Nähere Infos gibt es in folgender Gruppe, bei Telegram: https://t.me/Teelichter
