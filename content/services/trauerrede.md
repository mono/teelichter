---
title: 'Trauerrede'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'Trauerrede'
heroSubHeading: 'Die Rede zur Kungebung Januar 2021'
heroBackground: 'services/service1.jpg'
---
Sehr geehrte Angehörige, Freunde, Bekannte und Betroffene,
Liebe Trauergemeindschaft,
Es ist schön das ihr alle hierher gefunden habt, eigentlich wollte ich nichts sagen, weil mir vor Trauer und Wut die Worte fehlen, deshalb versuche ich mich kurz zu fassen.
Wir sind nicht alle, es fehlen uns über 40.000 Menschen die ein schlimmes Schicksal von uns
genommen hat. Ich darf an dieser Stelle auch nicht über den Mitschuldigen schweigen.
Ein Europa das sich selbst als Friedensnobelpreisträger kürt, dass aber durch unterlassene
Hilfeleistung, ein Dublin II abkommen, Menschen unwürdige Bedingungen in den Lagern und ein
latenten Rassismus die Mitschuld an dem Tod so vieler Menschen trägt.


Auch viele Familienmitglieder der gestorbenen können heute nicht hier sein, weil sie nichts von dieser Trauerfeier wissen, weil sie weit weg wohnen, oder weil sie sich vielleicht noch in einem Land aufhalten, wo Krieg und Armut auch ihr Leben bedrohen. Die meisten dieser gestorbenen haben keine Anständige Beerdigung erhalten, einige sind sogar nie gefunden worden oder in Massengräbern verscharrt worden. Es macht mich wütend und hilflos, wenn von einem Versagen der Politik in der Flüchtlingsfrage gesprochen wird. Das ist kein Versagen, das ist ein Kalkül wage ich zu behaupten. Dieses große sterben geht schon seit 1993 los und es hört nicht auf. Diese Toten werden nicht die letzten sein. Die Wähler die die CDU, deren christlich ein Hohn für die Menschlichkeit ist und ihnen aberkannt werden sollte, die SPD die nicht so sehr sozial ist, gewählt haben, wissen um deren Haltung in der Flüchtlingsfrage und wählen sie trotzdem.


Wie viele von den von uns gegangen kennen wir beim Namen? Sie erinnern sich vielleicht an Alan Kurdi, seine Familie ertrank im Mittelmeer und nur der Vater überlebte. Sie kennen vielleicht den von der Polizei erschossenen Halim Dener, ein Jugendlicher der aus der heutigen Krisenregion Kurdistan zu uns kam. Sie haben vielleicht von Laya-Alama Conde gehört, ein Mensch der in Polizeigewahrsam durch eine Polizeimaßnahme erstickt ist. Oder der Fall in Dresden von Oury Jalloh der in Polizeigewahrsam in Flammen aufging. Ich könnte ihnen noch einige Fälle nennen, von über Bord geworfenen Flüchtlingen, erschossenen Flüchtlingen und unzähligen ertrunkenen Flüchtlingen, die zu Retten die EU praktisch verbietet. Auch Neonazis haben einige getötet, das möchte ich nicht verschweigen, ich will aber ausdrücklich darauf hinweisen, das die meisten gestorben sind durch die Tatenlosigkeit der politischen Mitte. Wenn ich höhnisch wäre, würde ich Extreme Mitte sagen.
Ich könnte vermutlich noch Tausende Namen der gestorbenen aus dem Buch: Todesursache Flucht-
eine unvollständige Liste- vorlesen, aber dazu fehlt nicht nur die Zeit, sondern auch die Kraft.
Wie schrecklich ist es, das dieses Gedenken nicht einmal den einzelnen Würdigen kann, nur ein mickriges Teelicht für ein Drama was sich in oder vor Europa abgespielt hat.


Ich möchte jedoch nicht nur Anklagen, sondern auch Trauern mit euch.
Wir befinden uns in einer Pandemie, vielleicht hat der ein oder andere nun plötzlich Zeit, zuhause über die Welt und wie sie Organisiert ist, nachzudenken. Ich weiß das ich mich hier an eigentlich die Falschen richte, den wenn ihr hier steht, habt ihr vermutlich noch Mitgefühlt und euch ist nicht alles egal.
Together we are Bremen, Pro Asyl, Seebrücke. Karawane, der Flüchtlingsrat, die Rojava-Aktiven, viele Linke und Linksradikale nehmen diese Zustände war, und versuchen daran etwas zu ändern, ich danke euch dafür.
Wenn ihr nach Hause geht, vergesst diese Menschen nicht und auch die, die zukünftig sterben
werden mit der Billigung der Politik. Wenn ihr noch Kraft, Geld oder Zeit habt, organisiert euch und solidarisiert euch. Wir sind nicht alleine.
Ich bitte die Trauergemeindschaft nun eine Schweigeminute einzulegen.
