---
title: 'Wohnraum Spenden'
date: 2018-11-28T15:15:26+10:00
icon: 'services/service-icon-3.png'
featured: true
draft: false
heroHeading: 'Wohnraumspenden'
heroSubHeading: 'Damit Menschen die geflohen sind und kein Hartz bekommen leben können'
---
Spenden

Together we are Bremen braucht REGELMÄSSIGE Spenden. 
Bitte helft uns, Selbstorganisierung und die Kämpfe für Bleiberecht und gegen Rassismus hier in Bremen fortführen
zu können

Spendet über ein privates Konto (schickt uns eine Mail für die Kontodaten) oder 
das Vereinskonto von BreSoC (BremenSolidarityCenter e.V.).

Die Daten für das BreSoC-Konto:
Empfänger: BreSoC (Bremen Solidarity Centre) e.V.

IBAN: DE92 4306 0967 2074 0487 00

Bank: GLS Bank

Verwendungszweck: Spende TWAB