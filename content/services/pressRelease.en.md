---
title: 'Press Release'
date: 2018-11-28T15:14:39+10:00
icon: 'services/service-icon-2.png'
featured: true
draft: false
heroHeading: 'Press Release'
heroSubHeading: 'Our PR from Januar 2021 but just autotranslation'
heroBackground: 'services/service2.jpg'
---
1. Memorial rally for the victims of Fortress Europe
2. Date of promotion: 23.01.2021 
3. Location: Bürgerweide Bremen 
4. Time: 5-8 pm

## This is just autotranslation

In the book: "Todesursache Flucht - eine unvollständige Liste" (Reasons for dying on flee - an incomplete list)
35,500 dead refugees are listed. An unimaginably large number. The list covers the period from 1993 - 22.06.2018. 
Meanwhile, the number has grown to over 40,000. The number of unreported cases is much higher.
However, it is here not just a number, but about people whose death is accepted by the Nobel Peace Prize winner EU
has been taken. While the corona pandemic is currently exacerbating the situation for people seeking protection 
worldwide and conditions Fortress Europe continues to insist on its restrictive policy of isolation. Every day, 
thousands of people flee war, persecution, torture, slavery, violence and the loss of their Livelihood. 
And every day people die on this flight.
It has been happening for years at the external borders of the European Union, an association of states that was 
awarded the Nobel Peace Prize in 2010. But those seeking protection do not die from smuggling organizations, 
forces of nature, accidents or the sea. They die, because the EU's policy is on isolation, deterrence and cooperation 
with border protection agencies and Coast guards that violate human rights, instead of relying on assistance, 
solidarity and rescue. The leaders of the European states are only interested in keeping the EU borders closed. 
Meter-high barbed wire fences are erected, military operations are coordinated and millions of euros are being spent 
to EU riparian states that are supposed to prevent the entry of refugees. That this usually involves violence and 
people are deprived of their most basic rights, it seems to those responsible unproblematic. Because the result counts.
Since 2016, the numbers of those who have fled Europe after months of fleeing under terrible experiences, 
fallen sharply, as European politicians like to emphasize with pride. However, the percentage of people who die on 
their way is higher than ever. An often concealed fact in political statistics. But human lives are still at stake! 
People seeking protection who nevertheless make it over the walls of Fortress Europe can only make it under it is very 
difficult to apply for asylum. They are housed in inhumane conditions, imprisoned, crammed into camps, controlled and 
with the brutality of an inhuman asylum policy and the racist and often deadly police practice. They are simply 
treated as third-class people.
A human rights-oriented refugee policy looks different! To draw attention to the conditions at the external borders 
of Europe and beyond and to To commemorate deceased people and to make the victims of "Fortress Europe" visible,
yesterday, on Saturday, January 23, 2021, a group of civilian individuals called to 35,500 tealights on the Bürgerweide.
This call was followed in the course of the evening by around 200 people, who together served glasses and filled paper 
bags with lights. After two hours of intense and impressive collaboration, we began the short rally in which the 
initiator of the action said a few words about the connections between flight and European Foreign policy spoke.
The speech was followed by a minute's silence. At that time, there were already over 15,000 lights.
It offered the participants as well as some curious passers-by a very moving A sight that made it clear that the number
of victims of failed European policies cannot be overlooked. In a joint statement, the organizers state: We condemn an
EU that betrays its own values and tolerates numerous human rights violations instead of safe To create entry 
opportunities for those seeking protection. We condemn a confederation of states which is committed to maintaining his 
tough demarcation policy, among others, the border protection agency FRONTEX, which legitimizes by the EU Council 
Regulation carries out violent pushbacks. We condemn the detention of rescue ships, the suspension of international 
law of the sea and the return of refugees to so-called 'safe third countries'.

We do not want "Fortress Europe", but demand an EU that is immune to war, persecution and Displacement protects fleeing
people and welcomes them unconditionally. We expect the European asylum system to is subject to humanitarian principles. 
We call for the creation of safe and state-funded escape routes, which Dissolution of the 
European border management agency Frontex, as well as the abolition of all camps and the suspension of deportations.
And we call for an EU foreign policy that effectively supports, protects and combats the causes of flight. 

1. Short: 
2. "No to Fortress Europe!" 
3. "Fighting the causes of flight!" 
4. "Abolish all camps!"

We need more cities and municipalities that show their willingness to take in refugees.
We need more courageous helpers to save lives. We need more activists to draw attention to the situation.
And we need every single supporter who stands up every day against incitement, the brutalization of our society, 
the shift to the right and the propaganda against minorities and shows solidarity with the struggles of people worldwide. 
Because every single person has a right to flight, to free development and a life in security and peace.
No matter where and no matter how long. Regardless of origin, gender, religion or sexual orientation. It is up to us 
to defend and secure this right together! Thanks to all participants for their commitment, upholding human values and 
constant commitment to human rights. That was an important sign.
