---
title: 'Background Information'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-6.png'
draft: false
featured: true
weight: 2
heroHeading: 'first Flyer '
heroSubHeading: 'from Jan 2021'
heroBackground: 'work/flyer1.jpg'
---
In the book: "Todesursache Flucht - eine unvollständige Liste" 35,500 dead refugees are listed - an unimaginably large number. The list covers the period from 1993 - 22.06.2018. In the meantime, the number has grown to over 40,000. The number of unreported cases is much higher. However, this is not a mere number, but people whose deaths were accepted by the EU as a Nobel Peace Prize winner approvingly. 
On 23.01.21 from 17-20 clock a group of civilian individuals would like to commemorate the dead on the Bremen Bürgerweide. For this purpose we will place and light 35,500 tea lights on the Bürgerweide.
We invite all of you to participate in the commemoration of those who died on the run. Please bring candles, lighters and glasses and help to build and light the sea of lights.
Especially glasses for the tea lights are needed. Collection points have been set up for this purpose. More information is available in the following group, on Telegram: https://t.me/Teelichter
