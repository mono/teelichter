---
title: 'Mourning Speech'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'Eulogy'
heroSubHeading: 'The speech january 2021'
heroBackground: 'services/service1.jpg'
---
Dear relatives, friends, acquaintances and those affected, Dear mourners, It is
nice that you all have found here, actually I wanted to say nothing, because I
lack the words before grief and rage, therefore I try to be brief. We are not
all, we are missing over 40,000 people that a bad fate has taken from us. has
taken from us. At this point I must also not be silent about the complicit. A
Europe that chooses itself as a Nobel Peace Prize laureate, but that through
failure to provide Dublin II agreement, undignified conditions in the camps and
latent racism. latent racism is partly to blame for the death of so many people.


Also many family members of the deceased cannot be here today because they do
not know about this funeral service, because they live far away, or because they
may still be in a country where war and poverty also threaten their lives. Most
of those who died have not received a decent burial, some have even never been
found or buried in mass graves. It makes me angry and helpless when people talk
about a failure of politics in the refugee issue. This is not a failure, this is
a calculation, I dare to say. This great dying has been going on since 1993 and
it does not stop. These deaths will not be the last. The voters who voted for
the CDU, whose Christianity is a mockery of humanity and should be revoked, and
the SPD, which is not so very social, know about their position on the refugee
issue and vote for them anyway.


How many of those we have left do we know by name? You may remember Alan Kurdi,
his family drowned in the Mediterranean and only the father survived. You may
know Halim Dener, a youth who was shot by the police and came to us from what is
now the crisis region of Kurdistan. You may have heard of Laya-Alama Conde, a
person who suffocated in police custody as a result of police action. Or the
case in Dresden of Oury Jalloh who went up in flames in police custody. I could
tell you some more cases of refugees thrown overboard, refugees shot and
countless drowned refugees, which the EU practically forbids to save. Also
neo-Nazis have killed some, I do not want to hide that, but I want to point out
that most of them died because of the inaction of the political center. If I
were sneering, I would say extreme center.


I could probably still thousands of names of the died from the book: cause of
death flight - an incomplete list - read out. an incomplete list- but I don't
have not only the time but also the strength. How terrible it is that this
commemoration cannot even dignify the individual, only a puny tea light for a
drama which has taken place in or before Europe. However, I would like not only
to accuse, but also to mourn with you. We are in a pandemic, maybe one or the
other now suddenly has time to think at home about the world and how it is
organized. I know that I am actually addressing the wrong people, because if you
are standing here, you probably still have sympathy and you do not care about
everything. Together we are Bremen, Pro Asyl, Seebrücke. Karawane, the Refugee
Council, the Rojava activists, many leftists and left-wing radicals are aware of
these conditions and are trying to change something, I thank you for that. When
you go home, don't forget these people and also those who will die in the
future. with the approval of politics. If you still have strength, money or
time, organize yourselves and show solidarity. We are not alone.

I ask the mourners to observe a minute of silence.


