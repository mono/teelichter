---
title: 'Pressemitteilung'
date: 2018-11-28T15:14:39+10:00
icon: 'services/service-icon-2.png'
featured: true
draft: false
heroHeading: 'Pressemitteilung'
heroSubHeading: 'Unsere PM vom Januar 2021'
heroBackground: 'services/service2.jpg'
---
Gedenkkundgebung für die Opfer der Festung Europa 
Datum der Aktion: 23.01.2021 
Ort: Bürgerweide Bremen 
Zeit: 17-20 Uhr 

In dem Buch: „Todesursache Flucht – eine unvollständige Liste “ sind 35.500 tote Geflüchtete aufgelistet..Eine unvorstellbar große Zahl. Die Liste umfasst den Zeitraum von 1993 - 22.06.2018. Mittlerweile ist die Zahl auf über 40.000 angewachsen. Die Dunkelziffer liegt weitaus höher. Jedoch handelt es sich hier nicht um eine bloße Zahl, sondern um Menschen, deren Tod von der Friedensnobelpreisträgerin EU billigend in Kauf genommen wurde. 
Während derzeit die Corona-Pandemie die Situation für schutzsuchende Menschen weltweit weiter verschärft und Zustände schafft, die humanitäre Katastrophen befeuern, besteht die Festung Europa weiterhin auf ihrer restriktiven Abschottungspolitik. 
Täglich fliehen tausende Menschen vor Krieg, Verfolgung, Folter, Sklaverei, Gewalt und dem Verlust ihrer Existenzgrundlage. Und täglich kommen Menschen auf dieser Flucht ums Leben. Es geschieht seit Jahren an den Außengrenzen der Europäischen Union, einem Staatenverbund, der 2010 mit dem Friedensnobelpreis ausgezeichnet wurde. Doch Schutzsuchende sterben nicht durch Schlepperorganisationen, Naturgewalten, Unfälle oder das Meer. Sie sterben, weil die EU-Politik auf Abschottung, Abschreckung sowie Kooperation mit Grenzschutzagenturen und menschenrechtsverletzenden Küstenwachen, anstatt auf Hilfeleistung, Solidarität und Rettung setzt. Die Verantwortlichen der europäischen Staaten sind allein daran interessiert, die EU Grenzen dicht zu halten. Es werden meterhohe Stacheldrahtzäune errichtet, militärische Einsätze koordiniert und Millionenbeträge an EU- Anrainerstaaten überwiesen, die die Einreise von Flüchtenden unterbinden sollen. Dass dabei meist Gewalt angewendet wird und die Menschen ihrer elementarsten Rechte beraubt werden, scheint für die Verantwortlichen unproblematisch. Denn das Ergebnis zählt. Seit 2016 sind die Zahlen derjenigen, die Europa nach monatelanger Flucht unter furchtbaren Erlebnissen erreichen, stark gesunken, wie europäische Politiker*innen gern mit Stolz betonen. Jedoch ist die Zahl derjenigen Menschen, die auf ihrem Weg ums Leben kommen prozentual gesehen hoch wie nie. Eine oft verschwiegene Tatsache in politischen Statistiken. Doch es geht noch immer um Menschenleben! 
Schutzsuchende Menschen, die es dennoch über die Mauern der Festung Europa schaffen, können nur unter großen Schwierigkeiten Asyl beantragen. Sie werden unter menschenunwürdigen Bedingungen untergebracht, inhaftiert, in Lagern zusammengepfercht, kontrolliert und mit der Brutalität einer menschenverachtenden Asylpolitik und der rassistischen und häufig tödlichen Polizeipraxis konfrontiert. Sie 
werden schlicht als Menschen dritter Klasse behandelt. Eine menschenrechtsorientierte Geflüchtetenpolitik sieht anders aus! 
Um auf die Zustände an den Außengrenzen Europas und darüber hinaus aufmerksam zu machen und den auf der Flucht gestorbenen Menschen zu gedenken sowie die Opfer der "Festung Europa" sichtbar zu machen, hat gestern, am Samstag den 23. Januar 2021, eine Gruppe ziviler Einzelpersonen dazu aufgerufen, 35.500 Teelichter auf der Bürgerweide zu entzünden. 
Diesem Aufruf folgten im Laufe des Abends rund 200 Menschen, die gemeinsam Gläser und Papiertüten mit Lichtern füllten. Nach zwei Stunden intensiver und beeindruckender Zusammenarbeit begann die kurze Kundgebung, in der der Initiator der Aktion einige Worte zu den Zusammenhängen von Flucht und europäischer Außenpolitik sprach. Im Anschluss an die Rede folgte eine Schweigeminute. Zu diesem Zeitpunkt standen bereits über 15.000 Lichter. Es bot sich den Teilnehmer*innen sowie einigen neugierigen Passant*innen ein sehr bewegender Anblick, der deutlich machte, dass die Zahl der Opfer verfehlter europäischer Politik nicht zu überblicken ist. 
In einem gemeinsamen Statement heißt es von Seiten der Veranstalter*innen: 
Wir verurteilen eine EU, die ihre eigenen Werte verrät und zahlreiche Menschenrechtsverstöße toleriert, statt sichere Einreisemöglichkeiten für Schutzsuchende zu schaffen. Wir verurteilen einen Staatenbund, der sich zur Aufrechterhaltung seiner knallharten Abgrenzungspolitik unter anderem der Grenzschutzagentur FRONTEX, welche legitimiert durch die EU-Ratsverordnung gewaltsame Pushbacks durchführt, behilft. Wir verurteilen das Festsetzen von Rettungsschiffen, das Aussetzen internationalen Seerechts sowie die Zurückzuführung von Geflüchteten in sogenannte „sichere Drittstaaten “. 
Wir wollen keine "Festung Europa", sondern fordern eine EU, die die vor Krieg, Verfolgung und Vertreibung fliehende Menschen schützt und sie bedingungslos aufnimmt. Wir erwarten, dass das europäische Asylsystem humanitären Grundsätzen unterworfen wird. Wir fordern das Schaffen sicherer und staatlich finanzierter Fluchtwege, das Auflösen der europäischen Grenzschutzagentur Frontex, sowie die Abschaffung aller Lager und das Aussetzen von Abschiebungen. Und wir fordern eine EU-Außenpolitik, die wirksam unterstützt, schützt und Fluchtursachen bekämpft. 
Kurz: 
„Nein zur Festung Europa! “ 
„Fluchtursachen bekämpfen! “ 
„Alle Lager abschaffen! “ 

Wir brauchen mehr Städte und Gemeinden, die ihre Bereitschaft zur Aufnahme von Geflüchteten zeigen. Wir brauchen mehr mutige Helfer*innen, die Leben retten. Wir brauchen mehr Aktivist*innen, die auf die Situation aufmerksam machen. Und wir brauchen jeden einzelnen Unterstützer sowie jede einzelne Unterstützerin, die tagtäglich gegen Hetze, die Verrohung unserer Gesellschaft, den Rechtsruck und die Stimmungsmache gegen Minderheiten eintritt und Solidarität mit den Kämpfen von Menschen weltweit zeigt. Denn jede*r einzelne Mensch hat ein Recht auf Flucht, auf freie Entfaltung sowie ein Leben in Sicherheit und Frieden. Egal wo und egal wie lange. Unabhängig von Herkunft, Geschlecht, Religion oder sexueller Orientierung. Es liegt an uns, dieses Recht gemeinsam zu verteidigen und sicherzustellen! 
Danke an alle Teilnehmer*innen für das Engagement, das Hochhalten menschlicher Werte und den stetigen Einsatz für Menschenrechte. Das war ein wichtiges Zeichen. 
