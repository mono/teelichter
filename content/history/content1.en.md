---
title: 'How all started'
date: 2018-12-06T09:29:16+10:00
weight: 4
background: ''
align: right
---

Once upon a time, long ago... This is how fairy tales begin, but unfortunately the drama at the border of Europe is not a fairy tale. Also, it is not about the people who do this action, but about the victims who still die because of failure to provide assistance and lack of safe escape routes. People continue to die, they drown in the Mediterranean, commit suicide in camps and prisons or are killed by racist police/border guards/neo-Nazis. We want to make visible this high number of tragic but avoidable victims of racist policies.
